public class DependencyB
{
		
	private DependencyC _dependencyC;
		
	@Inject
	public DependencyB(DependencyC dependencyC)
	{
		_dependencyC = dependencyC;
		System.out.println("DependencyB with one parameter");
	}
	
	public DependencyB(int one, int two, int three)
	{
		System.out.println("Greediest Constructor");
	}
}