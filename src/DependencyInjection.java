import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class DependencyInjection {
	
	private Map<Class<?>, Class<?>> bindingsToClasses;
	
	public DependencyInjection() {
		bindingsToClasses = new HashMap<Class<?>, Class<?>>();
	}
	
	public void bindTo(Class<?> from, Class<?> to) {
		if (from.isInterface() && !to.isInterface()) {
			bindingsToClasses.put(from,  to);	
		}
	}

	public Object inject(Class<?> c) {
		
		if (bindingsToClasses.containsKey(c)) {
			c = bindingsToClasses.get(c);
		}
		
		if (c.isPrimitive()) {
			return getPrimative(c);
		}
		
		Constructor<?> greediestCon = getGreediestConstructor(c);
		Object[] instantiatedParams = instantiateParams(greediestCon);
		
		try {
			return greediestCon.newInstance(instantiatedParams);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		return "Not Valid";
	}
	
	private Object[] instantiateParams(Constructor<?> con) {
		
		Class<?>[] paramClasses = (Class<?>[]) con.getGenericParameterTypes();		
		Object[] instantiatedParams = new Object[paramClasses.length];
		for(int i = 0; i < paramClasses.length; i++) {
			instantiatedParams[i] = inject(paramClasses[i]);
		}
		return instantiatedParams;
	}
	
	private Object getPrimative(Class<?> c) {
		switch(c.getName()) {
		case "int": 
			return (int) 0;
		case "double": 
			return (double) 0.0;
		case "float": 
			return (float) .0;
		case "boolean": 
			return true;
		case "char": 
			return (char) 0;
		case "long": 
			return (long) 0;
		case "short": 
			return (short) 0;
		default: 
			return (byte) 0;
		}
	}
	
	private Constructor<?> getGreediestConstructor(Class<?> c) {
		Constructor<?>[] cons = c.getConstructors();		
		int maxParams = Integer.MIN_VALUE;
		int maxConsIndex = 0;
		for (int i = 0; i < cons.length; i++) {	
			if (cons[i].isAnnotationPresent(Inject.class)) {
				return cons[i];
			}
			
			Type[] params = cons[i].getParameterTypes();
			
			if (params.length > maxParams) {
				maxParams = params.length;
				maxConsIndex = i;
			}
		}
		return cons[maxConsIndex];
	}
}
