import java.lang.reflect.Constructor;

public class Main {

	public static void main(String[] args) {
		DependencyInjection di = new DependencyInjection();
		di.bindTo(DependencyC.class, DependencyCImplementationB.class);
		System.out.println(di.inject(ParentClass.class).toString());	
	}
}
