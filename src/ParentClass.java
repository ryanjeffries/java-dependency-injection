public class ParentClass 
{
	DependencyA _dependencyA;
	DependencyB _dependencyB;
	private String _constructor;
	
	public ParentClass(DependencyA a, DependencyB b) {
		_dependencyA = a;
		_dependencyB = b;
		_constructor = "Parent Two Constructors with Dependency A and Dependnecy B";
	}
	
	public ParentClass() {
		_constructor = "Parent with Zero Constructors"; 
	}
	
	public String toString() {
		return _constructor;
	}
}