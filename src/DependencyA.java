public class DependencyA 
{	
	private int _one;
	
	@Inject()
	public DependencyA(int one)
	{
		_one = one;
	}	
}